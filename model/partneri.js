const Sequelize = require("sequelize");
const sequelize = require("../config");

class Partneri extends Sequelize.Model { }
Partneri.init(
    {
        ime: {
            type: Sequelize.STRING,
            allowNull: false
        },
        oib: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        bankovniRacun: {
            type: Sequelize.STRING,
            allowNull: false
        },
        datumPocetka: {
            type: Sequelize.DATE
        },
        datumZavrsetka: {
            type: Sequelize.DATE
        },
        ulica: {
            type: Sequelize.STRING,
            allowNull: false
        },
        grad: {
            type: Sequelize.STRING,
            allowNull: false
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "partneri" }
);

module.exports = Partneri;
