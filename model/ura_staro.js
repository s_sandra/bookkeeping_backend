const Sequelize = require("sequelize");
const sequelize = require("../config");

class Ura extends Sequelize.Model {}

Ura.init(
    {
        porezniObveznik: {
            type: Sequelize.STRING
        },
        naziv: {
            type: Sequelize.STRING
        },
        adresa: {
            type: Sequelize.STRING
        },
        brojcanaOznaka: {
            type: Sequelize.INTEGER
        },
        pdvOib: {
            type: Sequelize.INTEGER
        },
        redBroj: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        broj: {
            type: Sequelize.INTEGER
        },
        datum: {
            type: Sequelize.DATE
        },
        sjediste: {
            type: Sequelize.STRING
        },
        pdvIdOib: {
            type: Sequelize.INTEGER
        },
        poreznaOsnivica5: {
            type: Sequelize.INTEGER
        },
        poreznaOsnivica13: {
            type: Sequelize.INTEGER
        },
        poreznaOsnivica25: {
            type: Sequelize.INTEGER
        },
        ukupniIznosPdv: {
            type: Sequelize.INTEGER
        },
        ukupno: {
            type: Sequelize.INTEGER
        },
        duzan: {
            type: Sequelize.INTEGER
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "ura" }
);

module.exports = Ura;
