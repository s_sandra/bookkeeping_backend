const Sequelize = require("sequelize");
const sequelize = require("../config");

class Porez extends Sequelize.Model { }
Porez.init(
    {
        porez: {
            type: Sequelize.INTEGER,
            allowNull: false,
            validate: {
                isAllowed(value) {
                    value = parseInt(value);
                    if (
                        !(
                            value == 25 ||
                            value == 13 ||
                            value == 5 ||
                            value == 0
                        )
                    ) {
                        throw new Error("Porez mora biti 25, 13, 5 ili 0!");
                    }
                }
            }
        },
        datumPocetka: {
            type: Sequelize.DATE,
            allowNull: false
        },
        datumZavrsetka: {
            type: Sequelize.DATE
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "porez" }
);

module.exports = Porez;
