const Sequelize = require("sequelize");
const sequelize = require("../config");

class Banka extends Sequelize.Model { }

Banka.init(
    {
        nazivBanke: {
            type: Sequelize.STRING
        },
        ziroRacun: {
            type: Sequelize.STRING
        },
        idUser: {
            type: Sequelize.INTEGER
        }
    },
    { sequelize, freezeTableName: true, modelName: "banka" }
);

module.exports = Banka;
