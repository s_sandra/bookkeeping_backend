const Sequelize = require("sequelize");
const sequelize = require("../config");

class Transakcija extends Sequelize.Model { }
Transakcija.init(
    {
        datumTransakcije: {
            type: Sequelize.DATE,
        },
        idIra: {
            type: Sequelize.INTEGER,
        },
        idUra: {
            type: Sequelize.INTEGER,
        },
        idBankovniIzvod: {
            type: Sequelize.INTEGER,
        },
    },
    { sequelize, freezeTableName: true, modelName: "transakcija" }
);

module.exports = Transakcija;
