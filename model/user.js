const Sequelize = require("sequelize");
const sequelize = require("../config");

class User extends Sequelize.Model {}
User.init(
    {
        korisnickoIme: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        lozinka: {
            type: Sequelize.STRING,
            allowNull: false
        },
        ime: {
            type: Sequelize.STRING,
            allowNull: false
        },
        prezime: {
            type: Sequelize.STRING,
            allowNull: false
        },
        oib: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        poduzece: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    { sequelize, modelName: "user" }
);

module.exports = User;
