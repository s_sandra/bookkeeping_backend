const Sequelize = require("sequelize");
const sequelize = require("../config");

class BankovniIzvod extends Sequelize.Model { }

BankovniIzvod.init(
    {
        ziroRacun: {
            type: Sequelize.STRING
        },
        brojIzvoda: {
            type: Sequelize.INTEGER
        },
        datumIzvoda: {
            type: Sequelize.DATEONLY
        },
        duguje: {
            type: Sequelize.DECIMAL(10, 2)
        },
        potrazuje: {
            type: Sequelize.DECIMAL(10, 2)
        },
        veznaOznaka: {
            type: Sequelize.STRING
        },
        opisKnjizenja: {
            type: Sequelize.STRING
        },
        idBanka: {
            type: Sequelize.INTEGER
        },
        idPartner: {
            type: Sequelize.INTEGER
        },
        idKategorijePrihoda: {
            type: Sequelize.STRING
        },
        idKategorijeTroskova: {
            type: Sequelize.STRING
        },
        idUser: {
            type: Sequelize.INTEGER
        },

    },
    { sequelize, freezeTableName: true, modelName: "bankovniIzvod" }
);

module.exports = BankovniIzvod;