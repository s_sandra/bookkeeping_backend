const Sequelize = require("sequelize");
const sequelize = require("../config");

class Ira extends Sequelize.Model { }

Ira.init(
    {
        brojRacuna: {
            type: Sequelize.STRING
        },
        datumRacuna: {
            type: Sequelize.DATEONLY
        },
        datumDospijeca: {
            type: Sequelize.DATEONLY
        },
        iznos: {
            type: Sequelize.DECIMAL(10, 2)
        },
        ostatak: {
            type: Sequelize.DECIMAL(10, 2)
        },
        idPartner: {
            type: Sequelize.INTEGER,
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
    },
    { sequelize, freezeTableName: true, modelName: "ira" }
);

module.exports = Ira;