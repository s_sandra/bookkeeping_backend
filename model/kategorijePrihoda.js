const Sequelize = require("sequelize");
const sequelize = require("../config");

class KategorijePrihoda extends Sequelize.Model { }
KategorijePrihoda.init(
    {
        opis: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "kategorijePrihoda" }
);

module.exports = KategorijePrihoda;
