const Sequelize = require("sequelize");
const sequelize = require("../config");

class Ira extends Sequelize.Model {}

Ira.init(
    {
        porezniObveznik: {
            type: Sequelize.STRING
        },
        naziv: {
            type: Sequelize.STRING
        },
        adresa: {
            type: Sequelize.STRING
        },
        brojcanaOznaka: {
            type: Sequelize.INTEGER
        },
        pdvOib: {
            type: Sequelize.INTEGER
        },
        redBroj: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        broj: {
            type: Sequelize.INTEGER
        },
        datum: {
            type: Sequelize.DATE
        },
        sjediste: {
            type: Sequelize.STRING
        },
        pdvIdOib: {
            type: Sequelize.INTEGER
        },
        iznosPdv: {
            type: Sequelize.INTEGER
        },
        duzan: {
            type: Sequelize.INTEGER
        },
        tuzemniPrijenos: {
            type: Sequelize.STRING
        },
        isporukeClanicama: {
            type: Sequelize.STRING
        },
        isporukeEu: {
            type: Sequelize.STRING
        },
        obavljanjeEu: {
            type: Sequelize.STRING
        },
        obavljanjeRh: {
            type: Sequelize.STRING
        },
        sastavljanjeClanici: {
            type: Sequelize.STRING
        },
        isporukeNpsEu: {
            type: Sequelize.STRING
        },
        uTuzemstvu: {
            type: Sequelize.STRING
        },
        izvozneIsporuke: {
            type: Sequelize.STRING
        },
        ostalaOslobodjenja: {
            type: Sequelize.STRING
        },
        userId: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "ira" }
);

module.exports = Ira;
