const Sequelize = require("sequelize");
const sequelize = require("../config");

class Kpi extends Sequelize.Model { }
Kpi.init(
    {
        nazivDjelatnosti: {
            type: Sequelize.STRING
        },
        imePrezime: {
            type: Sequelize.STRING
        },
        adresaPrebivalista: {
            type: Sequelize.STRING
        },
        oibPoduzetnika: {
            type: Sequelize.INTEGER
        },
        sifraDjelatnosti: {
            type: Sequelize.INTEGER
        },
        rbr: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nadnevakPrimitka: {
            type: Sequelize.STRING
        },
        brojTemeljnice: {
            type: Sequelize.INTEGER
        },
        opisIsprava: {
            type: Sequelize.TEXT
        },
        primiciUGotovini: {
            type: Sequelize.INTEGER
        },
        primiciNaZiroRacun: {
            type: Sequelize.INTEGER
        },
        primiciUNaravi: {
            type: Sequelize.INTEGER
        },
        pdvUPrimicima: {
            type: Sequelize.INTEGER
        },
        ukupniPrimici: {
            type: Sequelize.INTEGER
        },
        izdaciUGotovini: {
            type: Sequelize.INTEGER
        },
        izdaciPutemZiroRacuna: {
            type: Sequelize.INTEGER
        },
        izdaciUNaravi: {
            type: Sequelize.INTEGER
        },
        pdvUIzdacima: {
            type: Sequelize.INTEGER
        },
        izdaciIzCl33: {
            type: Sequelize.INTEGER
        },
        ukupniDopusteniIzdaci: {
            type: Sequelize.INTEGER
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "kpi" }
);

module.exports = Kpi;
