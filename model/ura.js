const Sequelize = require("sequelize");
const sequelize = require("../config");

class Ura extends Sequelize.Model { }

Ura.init(
    {
        brojRacuna: {
            type: Sequelize.STRING
        },
        datumRacuna: {
            type: Sequelize.DATEONLY
        },
        datumDospijeca: {
            type: Sequelize.DATEONLY
        },
        iznos: {
            type: Sequelize.DECIMAL(10, 2)
        },
        ostatak: {
            type: Sequelize.DECIMAL(10, 2)
        },
        idPartner: {
            type: Sequelize.INTEGER,
        },
        idUser: {
            type: Sequelize.INTEGER,
        },
    },
    { sequelize, freezeTableName: true, modelName: "ura" }
);

module.exports = Ura;