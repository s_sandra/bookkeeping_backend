const Sequelize = require("sequelize");
const sequelize = require("../config");

class KategorijeTroskova extends Sequelize.Model { }
KategorijeTroskova.init(
    {
        opis: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        idUser: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    { sequelize, freezeTableName: true, modelName: "kategorijeTroskova" }
);

module.exports = KategorijeTroskova;
