const KategorijePrihoda = require("../model/kategorijePrihoda");

describe("KategorijePrihoda model tests", () => {
    it("Creates table", done => {
        KategorijePrihoda.sync({ force: true }).then(() => {
            KategorijePrihoda.create({
                opis: "Mito i korupcija",
                idUser: 2
            })
                .then(() => done())
                .catch(err => done(new Error(err)));
        });
    });
});
