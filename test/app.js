let expect  = require("chai").expect;
let axios = require("axios");
const PORT = 8000;

describe("Bookkeeping server", () => {
    it("returns status 200 OK", () => {
        axios.get("http://127.0.0.1:8000/", (err, res, body) => {
            expect(res.statusCode).to.equal(200);
        });
    });
});