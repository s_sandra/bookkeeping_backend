const Sequelize = require("sequelize");
const Porez = require("../model/porez");

describe("Porez model tests", () => {
    it("Creates table", done => {
        Porez.sync({ force: true }).then(() => {
            Porez.create({
                porez: 25,
                datumPocetka: Sequelize.fn("NOW"),
                datumZavrsetka: null,
                idUser: 2
            })
                .then(() => done())
                .catch(err => done(new Error(err)));
        });
    });
});
