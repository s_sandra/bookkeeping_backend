let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const Transakcija = require("../model/transakcija");

describe("Ira tests", () => {
    it("Creates table", done => {
        Transakcija.sync({ force: true }).then(() => {
            Transakcija.create({
                datumTransacije: Sequelize.fn("NOW"),
                idIra: 4,
                idUra: null,
                idBankovniIzvod: 87,
            });
            done();
        });
    });
});
