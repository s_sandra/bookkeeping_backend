let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const Ura = require("../model/ura");

describe("Ura tests", () => {
    it("Creates table", done => {
        Ura.sync({ force: true }).then(() => {
            Ura.create({
                brojRacuna: 2221,
                datumRacuna: Sequelize.fn("NOW"),
                datumDospijeca: null,
                iznos: 30220,
                ostatak: 220,
                idPartner: 4,
                idUser: 1,
            });
            done();
        });
    });
});
