const KategorijeTroskova = require("../model/kategorijeTroskova");

describe("KategorijeTroskova model tests", () => {
    it("Creates table", done => {
        KategorijeTroskova.sync({ force: true }).then(() => {
            KategorijeTroskova.create({
                opis: "Mito i korupcija",
                idUser: 1
            })
                .then(() => done())
                .catch(err => done(new Error(err)));
        });
    });
});
