let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const Ira = require("../model/ira");

describe("Ira tests", () => {
    it("Creates table", done => {
        Ira.sync({ force: true }).then(() => {
            Ira.create({
                brojRacuna: 21,
                datumRacuna: Sequelize.fn("NOW"),
                datumDospijeca: null,
                iznos: 300,
                ostatak: 20,
                idPartner: 5,
                idUser: 1,
            });
            done();
        });
    });
});
