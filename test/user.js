let expect = require("chai").expect;
const axios = require("axios");
const sequelize = require("../config");
const User = require("../model/user");
const bcrypt = require("bcryptjs");

describe("Database connection - User model", () => {
    it("Database connects properly", done => {
        sequelize.authenticate().then(() => done());
    });
    it("Creates user table", done => {
        User.sync({ force: true }).then(() => {
            let user = User.create({
                korisnickoIme: "samuel",
                lozinka: bcrypt.hashSync("samuel"),
                ime: "samuel",
                prezime: "savanovic",
                oib: 322132,
                poduzece: "cloudbis"
            });
            done();
            return user;
        });
    });
    it("Logins user", done => {
        axios
            .post("http://127.0.0.1:8000/api/login", {
                korisnickoIme: "samuel",
                lozinka: "samuel"
            })
            .then(res => {
                if (res.data.err) {
                    done(new Error(res.data.err));
                } else {
                    expect(res.data.user.id).to.equal(1);
                    done();
                }
            })
            .catch(err => done(new Error(err)));
    });
    it("Creates additional user using route", done => {
        axios
            .post("http://127.0.0.1:8000/api/create", {
                korisnickoIme: "sandra",
                lozinka: "sandra",
                ime: "sandra",
                prezime: "sabranovic",
                oib: 56671,
                poduzece: "cloudbis"
            })
            .then(res => {
                expect(res.data.id).to.equal(2);
                done();
            })
            .catch(err => done(new Error(err)));
    });
});
