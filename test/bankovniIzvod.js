let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const BankovniIzvod = require("../model/bankovniIzvod");

describe("Ira tests", () => {
    it("Creates table", done => {
        BankovniIzvod.sync({ force: true }).then(() => {
            BankovniIzvod.create({
                ziroRacun: "HR038092390482932",
                brojIzvoda: 21,
                datumIzvoda: Sequelize.fn("NOW"),
                duguje: 300,
                potrazuje: 2000,
                veznaOznaka: "244324",
                opisKnjizenja: "blablaaaa",
                idBanka: 3,
                idPartner: 2,
                idUser: 1,
                idKategorijePrihoda: 2,
                idKategorijeTroskova: 6,
            });
            done();
        });
    });
});
