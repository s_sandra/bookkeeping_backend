let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const Partneri = require("../model/partneri");

describe("Partneri tests", () => {
    it("Creates table", done => {
        Partneri.sync({ force: true }).then(() => {
            Partneri.create({
                ime: "infinum",
                oib: 312131,
                bankovniRacun: "HR0123231123",
                datumPocetka: Sequelize.fn("NOW"),
                datumZavrsetka: null,
                ulica: "Put Heroja 3",
                grad: "Gradovnik",
                idUser: 2
            });
            done();
        });
    });
    /* it("Get all partners request", done => {
         axios.get("http://127.0.0.1:8000/api/partners")
             .then(res => {
                 if (res.data.err) {
                     throw new Error(res.data.err);
                 } else {
                     expect(res.data.length).to.equal(1);
                     done();
                 }
             })
             .catch(err => {
                 done(err);
             });
     });*/
});
