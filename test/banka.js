let expect = require("chai").expect;
const axios = require("axios");

const Sequelize = require("sequelize");
const Banka = require("../model/banka");

describe("Banka tests", () => {
    it("Creates table", done => {
        Banka.sync({ force: true }).then(() => {
            Banka.create({
                nazivBanke: "erste",
                ziroRacun: "HR2709309327",
                idUser: 1
            });
            done();
        });
    });
});
