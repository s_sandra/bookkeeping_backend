const express = require("express");
const bodyParser = require("body-parser");

let routes = require("./routes");
let uraRoutes = require("./routes/ura");
let iraRoutes = require("./routes/ira");
let bankovniIzvodRoutes = require("./routes/bankovniIzvod");
let partnerRoutes = require("./routes/partner");
let kategorijePrihodaRoutes = require("./routes/kategorijePrihoda");
let kategorijeTroskovaRoutes = require("./routes/kategorijeTroskova");
let bankaRoutes = require("./routes/banka");
let transakcijaRoutes = require("./routes/transakcija");

require("dotenv").config();

const PORT = process.env.PORT || 8000;
const app = express();

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); //change to domain!
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method == "OPTIONS") {
        res.header(
            "Access-Control-Allow-Methods",
            "PUT, POST, PATCH, DELETE, GET"
        );
        return res.status(200).json({});
    }
    next();
});
app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.send("Hello");
});

app.use("/api", routes);
app.use("/api/ura", uraRoutes);
app.use("/api/ira", iraRoutes);
app.use("/api/bankovniIzvod", bankovniIzvodRoutes);
app.use("/api/partner", partnerRoutes);
app.use("/api/kategorijePrihoda", kategorijePrihodaRoutes);
app.use("/api/kategorijeTroskova", kategorijeTroskovaRoutes);
app.use("/api/banka", bankaRoutes);
app.use("/api/transakcija", transakcijaRoutes);

app.get("*", (req, res) => {
    res.redirect("/");
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
