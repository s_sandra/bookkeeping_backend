const Banka = require("../model/banka");

exports.index = (req, res, next) => {
    Banka.findAll()
        .then(banka => {
            res.status(200).json(banka);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    Banka.create({
        nazivBanke: values.nazivBanke,
        ziroRacun: values.ziroRacun,
        idUser: values.userId
    })
        .then(banka => {
            res.status(200).json(banka.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Banka.findOne({
        where: { id: req.params.bankaId, userId: req.body.userId }
    })
        .then(banka => {
            res.status(200).json(banka.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    Banka.update(
        {
            nazivBanke: values.nazivBanke,
            ziroRacun: values.ziroRacun,
            idUser: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.bankaId,
                idUser: values.userId
            }
        }
    )
        .then(affectedRows => {
            return Banka.findOne({
                where: { id: req.params.bankaId, idUser: values.userId }
            });
        })
        .then(banka => res.status(200).json(banka))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    Banka.destroy({
        where: {
            id: req.params.bankaId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.bankaId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
