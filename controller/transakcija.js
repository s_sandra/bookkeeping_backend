const Transakcija = require("../model/transakcija");
let Promise = require("promise");

exports.index = (req, res, next) => {
    Transakcija.findAll()
        .then(transakcija => {
            res.status(200).json(transakcija);
        })
        .catch(err => {
            res.status(500).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    let promises = [];
    if (values.idUra) {
        for (let i = 0; i < values.idUra.length; i++) {
            let newPromise = Transakcija.create({
                datumTransakcije: values.datumTransakcije,
                idUra: values.idUra[i],
                idIra: null,
                idBankovniIzvod: values.idBankovniIzvod
            });
            promises.push(newPromise);
        }
    } else {
        for (let i = 0; i < values.idIra.length; i++) {
            let newPromise = Transakcija.create({
                datumTransakcije: values.datumTransakcije,
                idUra: null,
                idIra: values.idIra[i],
                idBankovniIzvod: values.idBankovniIzvod
            });
            promises.push(newPromise);
        }
    }
    Promise.all(promises)
        .then(transakcije => {
            res.status(200).json(transakcije);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {

};

exports.delete = (req, res, next) => {

};