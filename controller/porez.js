const Porez = require("../model/porez");

exports.index = (req, res, next) => {
    Porez.findAll()
        .then(porez => {
            res.status(200).json(porez.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body.values;
    Porez.create({
        porez: values.porez,
        datumPocetka: values.datumPocetka,
        datumZavrsetka: values.datumZavrsetka,
        userId: values.userId
    })
        .then(porez => {
            res.status(200).json(porez.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Porez.findOne({
        where: { id: req.params.porezId, userId: req.body.userId }
    })
        .then(porez => {
            res.status(200).json(porez.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body.values;
    Porez.update(
        {
            porez: values.porez,
            datumPocetka: values.datumPocetka,
            datumZavrsetka: values.datumZavrsetka,
            userId: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.porezId,
                userId: values.userId
            }
        }
    )
        .then((rowsUpdated, [updatedRows]) => {
            res.status(200).json(updatedRows);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    Porez.destroy({
        where: {
            id: req.params.porezId,
            userId: req.body.userId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json({ message: "Deleted successfully!" });
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
