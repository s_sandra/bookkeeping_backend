const User = require("../model/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.login = (req, res, next) => {
    User.findOne({
        where: {
            korisnickoIme: req.body.korisnickoIme
        }
    })
        .then(data => {
            if (data) {
                bcrypt.compare(
                    req.body.lozinka,
                    data.lozinka,
                    (err, result) => {
                        if (result) {
                            const token = jwt.sign(
                                {
                                    korisnickoIme: data.korisnickoIme,
                                    id: data.id
                                },
                                process.env.JWT_KEY,
                                { expiresIn: "3h" }
                            );
                            res.status(200);
                            res.json({
                                user: data.get({ plain: true }),
                                token: token,
                                message: "Uspješna prijava!"
                            });
                        } else {
                            res.json({ err: "Netočna lozinka!" });
                        }
                        if (err) {
                            next(err);
                        }
                    }
                );
            } else {
                res.json({
                    err: `Korisnik sa korisničkim imenom ${
                        req.body.korisnickoIme
                    } ne postoji!`
                });
            }
        })
        .catch(err => {
            next(err);
        });
};

exports.create = (req, res, next) => {
    User.create({
        korisnickoIme: req.body.korisnickoIme,
        lozinka: bcrypt.hashSync(req.body.lozinka),
        ime: req.body.ime,
        prezime: req.body.prezime,
        oib: req.body.oib,
        poduzece: req.body.poduzece
    })
        .then(data => {
            res.status(200);
            res.json(data.get({ plain: true }));
        })
        .catch(err => {
            next(err);
        });
};
