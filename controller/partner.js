const Partneri = require("../model/partneri");

exports.index = (req, res, next) => {
    Partneri.findAll()
        .then(partners => {
            res.status(200).json(partners);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    Partneri.create({
        ime: values.ime,
        oib: parseInt(values.oib),
        bankovniRacun: values.bankovniRacun,
        datumPocetka: null,
        datumZavrsetka: null,
        ulica: values.ulica,
        grad: values.grad,
        idUser: values.userId
    })
        .then(partner => {
            res.status(200).json(partner.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Partneri.findOne({
        where: { id: req.params.partnerId, userId: req.body.userId }
    })
        .then(partner => {
            res.status(200).json(partner.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    Partneri.update(
        {
            ime: values.ime,
            oib: values.oib,
            bankovniRacun: values.bankovniRacun,
            datumPocetka: values.datumPocetka,
            datumZavrsetka: values.datumZavrsetka,
            ulica: values.ulica,
            grad: values.grad,
            idUser: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.partnerId,
                idUser: values.userId
            }
        }
    )
        .then(affectedRows => {
            return Partneri.findOne({
                where: { id: req.params.partnerId, idUser: values.userId }
            });
        })
        .then(partner => res.status(200).json(partner))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    Partneri.destroy({
        where: {
            id: req.params.partnerId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.partnerId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
