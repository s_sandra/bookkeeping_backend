const KategorijeTroskova = require("../model/kategorijeTroskova");

exports.index = (req, res, next) => {
    KategorijeTroskova.findAll()
        .then(kategorijeTroskova => {
            res.status(200).json(kategorijeTroskova);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    KategorijeTroskova.create({
        opis: values.opis,
        idUser: values.userId
    })
        .then(kategorijeTroskova => {
            res.status(200).json(kategorijeTroskova.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    KategorijeTroskova.findOne({
        where: { id: req.params.kategorijeTroskovaId, userId: req.body.userId }
    })
        .then(kategorijeTroskova => {
            res.status(200).json(kategorijeTroskova.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    KategorijeTroskova.update(
        {
            opis: values.opis,
            idUser: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.kategorijeTroskovaId,
                idUser: values.userId
            }
        }
    )
        .then(affectedRows => {
            return KategorijeTroskova.findOne({
                where: { id: req.params.kategorijeTroskovaId, idUser: values.userId }
            });
        })
        .then(kategorija => res.status(200).json(kategorija))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    KategorijeTroskova.destroy({
        where: {
            id: req.params.kategorijeTroskovaId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.kategorijeTroskovaId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
