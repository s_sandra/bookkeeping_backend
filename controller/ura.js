const Ura = require("../model/ura");

exports.index = (req, res, next) => {
    Ura.findAll()
        .then(ura => {
            res.status(200).json(ura);
        })
        .catch(err => {
            res.status(500).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    Ura.create({
        brojRacuna: values.brojRacuna,
        datumRacuna: values.datumRacuna,
        datumDospijeca: values.datumDospijeca,
        iznos: values.iznos,
        ostatak: values.ostatak,
        idPartner: values.idPartner,
        idUser: values.userId
    })
        .then(ura => {
            res.status(200).json(ura.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Ura.findOne({
        where: { id: req.params.uraId, userId: req.body.userId }
    })
        .then(ura => {
            res.status(200).json(ura.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    Ura.update({
        brojRacuna: values.brojRacuna,
        datumRacuna: values.datumRacuna,
        datumDospijeca: values.datumDospijeca,
        iznos: values.iznos,
        ostatak: values.ostatak,
        idPartner: values.idPartner,
        idUser: values.userId
    }, {
            returning: true,
            where: {
                id: req.params.uraId,
                idUser: values.userId
            }
        })
        .then(affectedRows => {
            return Ura.findOne({
                where: { id: req.params.uraId, idUser: values.userId }
            });
        })
        .then(ura => res.status(200).json(ura))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};


//should also delete transaction if is connected 
exports.delete = (req, res, next) => {
    Ura.destroy({
        where: {
            id: req.params.uraId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.uraId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
