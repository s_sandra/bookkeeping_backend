const KategorijePrihoda = require("../model/kategorijePrihoda");

exports.index = (req, res, next) => {
    KategorijePrihoda.findAll()
        .then(kategorijePrihoda => {
            res.status(200).json(kategorijePrihoda);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    KategorijePrihoda.create({
        opis: values.opis,
        idUser: values.userId
    })
        .then(kategorijePrihoda => {
            res.status(200).json(kategorijePrihoda.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    KategorijePrihoda.findOne({
        where: { id: req.params.kategorijePrihodaId, userId: req.body.userId }
    })
        .then(kategorijePrihoda => {
            res.status(200).json(kategorijePrihoda.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    KategorijePrihoda.update(
        {
            opis: values.opis,
            idUser: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.kategorijePrihodaId,
                idUser: values.userId
            }
        }
    )
        .then(affectedRows => {
            return KategorijePrihoda.findOne({
                where: { id: req.params.kategorijePrihodaId, idUser: values.userId }
            });
        })
        .then(kategorija => res.status(200).json(kategorija))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    KategorijePrihoda.destroy({
        where: {
            id: req.params.kategorijePrihodaId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.kategorijePrihodaId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
