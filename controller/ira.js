const Ira = require("../model/ira");

exports.index = (req, res, next) => {
    Ira.findAll()
        .then(ira => {
            res.status(200).json(ira);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    Ira.create({
        brojRacuna: values.brojRacuna,
        datumRacuna: values.datumRacuna,
        datumDospijeca: values.datumDospijeca,
        iznos: values.iznos,
        ostatak: values.ostatak,
        idPartner: values.idPartner,
        idUser: values.userId
    })
        .then(ira => {
            res.status(200).json(ira.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Ira.findOne({
        where: { id: req.params.iraId, userId: req.body.userId }
    })
        .then(ira => {
            res.status(200).json(ira.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body;
    Ira.update({
        brojRacuna: values.brojRacuna,
        datumRacuna: values.datumRacuna,
        datumDospijeca: values.datumDospijeca,
        iznos: values.iznos,
        ostatak: values.ostatak,
        idPartner: values.idPartner,
        idUser: values.userId
    }, {
            returning: true,
            where: {
                id: req.params.iraId,
                idUser: values.userId
            }
        })
        .then(affectedRows => {
            return Ira.findOne({
                where: { id: req.params.iraId, idUser: values.userId }
            });
        })
        .then(ira => res.status(200).json(ira))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

//should also delete transaction if is connected 
exports.delete = (req, res, next) => {
    Ira.destroy({
        where: {
            id: req.params.iraId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.iraId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
