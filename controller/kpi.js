const Kpi = require("../model/kpi");

exports.index = (req, res, next) => {
    Kpi.findAll()
        .then(kpi => {
            res.status(200).json(kpi.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body.values;
    Kpi.create({
        nazivDjelatnosti: values.nazivDjelatnosti,
        imePrezime: values.imePrezime,
        adresaPrebivalista: values.adresaPrebivalista,
        oibPoduzetnika: values.oibPoduzetnika,
        sifraDjelatnosti: values.sifraDjelatnosti,
        rbr: values.rbr,
        nadnevakPrimitka: values.nadnevakPrimitka,
        brojTemeljnice: values.brojTemeljnice,
        opisIsprava: values.opisIsprava,
        primiciUGotovini: values.primiciUGotovini,
        primiciNaZiroRacun: values.primiciNaZiroRacun,
        primiciUNaravi: values.primiciUNaravi,
        pdvUPrimicima: values.pdvUPrimicima,
        ukupniPrimici: values.ukupniPrimici,
        izdaciUGotovini: values.izdaciUGotovini,
        izdaciPutemZiroRacuna: values.izdaciPutemZiroRacuna,
        izdaciUNaravi: values.izdaciUNaravi,
        pdvUIzdacima: values.pdvUIzdacima,
        izdaciIzCl33: values.izdaciIzCl33,
        ukupniDopusteniIzdaci: values.ukupniDopusteniIzdaci,
        userId: values.userId
    })
        .then(kpi => {
            res.status(200).json(kpi.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.show = (req, res, next) => {
    Kpi.findOne({
        where: { id: req.params.kpiId, userId: req.body.userId }
    })
        .then(kpi => {
            res.status(200).json(kpi.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.update = (req, res, next) => {
    const values = req.body.values;
    Kpi.update(
        {
            nazivDjelatnosti: values.nazivDjelatnosti,
            imePrezime: values.imePrezime,
            adresaPrebivalista: values.adresaPrebivalista,
            oibPoduzetnika: values.oibPoduzetnika,
            sifraDjelatnosti: values.sifraDjelatnosti,
            rbr: values.rbr,
            nadnevakPrimitka: values.nadnevakPrimitka,
            brojTemeljnice: values.brojTemeljnice,
            opisIsprava: values.opisIsprava,
            primiciUGotovini: values.primiciUGotovini,
            primiciNaZiroRacun: values.primiciNaZiroRacun,
            primiciUNaravi: values.primiciUNaravi,
            pdvUPrimicima: values.pdvUPrimicima,
            ukupniPrimici: values.ukupniPrimici,
            izdaciUGotovini: values.izdaciUGotovini,
            izdaciPutemZiroRacuna: values.izdaciPutemZiroRacuna,
            izdaciUNaravi: values.izdaciUNaravi,
            pdvUIzdacima: values.pdvUIzdacima,
            izdaciIzCl33: values.izdaciIzCl33,
            ukupniDopusteniIzdaci: values.ukupniDopusteniIzdaci,
            userId: values.userId
        },
        {
            returning: true,
            where: {
                id: req.params.kpiId,
                userId: values.userId
            }
        }
    )
        .then((rowsUpdated, [updatedRows]) => {
            res.status(200).json(updatedRows);
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    Kpi.destroy({
        where: {
            id: req.params.kpiId,
            userId: req.body.userId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json({ message: "Deleted successfully!" });
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
};
