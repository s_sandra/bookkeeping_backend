const BankovniIzvod = require("../model/bankovniIzvod");

exports.index = (req, res, next) => {
    BankovniIzvod.findAll()
        .then(bankovniIzvod => {
            res.status(200).json(bankovniIzvod);
        })
        .catch(err => {
            res.status(500).json({ err: err });
            next(err);
        });
};

exports.create = (req, res, next) => {
    const values = req.body;
    BankovniIzvod.create({
        ziroRacun: values.ziroRacun,
        brojIzvoda: values.brojIzvoda,
        datumIzvoda: values.datumIzvoda,
        duguje: values.duguje,
        potrazuje: values.potrazuje,
        veznaOznaka: values.veznaOznaka,
        opisKnjizenja: values.opisKnjizenja,
        idBanka: values.idBanka,
        idPartner: values.idPartner,
        idKategorijePrihoda: values.konto,
        idKategorijeTroskova: values.konto,
        idUser: values.userId
    })
        .then(bankovniIzvod => {
            res.status(200).json(bankovniIzvod.get({ raw: true }));
        })
        .catch(err => {
            res.status(500).json({ err: err });
        });
};

exports.show = (req, res, next) => {
    BankovniIzvod.findOne({
        where: { id: req.params.bankovniIzvodId, userId: req.body.userId }
    })
        .then(bankovniIzvod => {
            res.status(200).json(bankovniIzvod.get({ raw: true }));
        })
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
}

exports.update = (req, res, next) => {
    const values = req.body;
    BankovniIzvod.update({
        ziroRacun: values.ziroRacun,
        brojIzvoda: values.brojIzvoda,
        datumIzvoda: values.datumIzvoda,
        duguje: values.duguje,
        potrazuje: values.potrazuje,
        veznaOznaka: values.veznaOznaka,
        opisKnjizenja: values.opisKnjizenja,
        idBanka: values.idBanka,
        idPartner: values.idPartner,
        idKategorijePrihoda: values.konto,
        idKategorijeTroskova: values.konto,
        idUser: values.userId
    }, {
            returning: true,
            where: {
                id: req.params.bankovniIzvodId,
                idUser: values.userId
            }
        })
        .then(affectedRows => {
            return BankovniIzvod.findOne({
                where: { id: req.params.bankovniIzvodId, idUser: values.userId }
            });
        })
        .then(izvod => res.status(200).json(izvod))
        .catch(err => {
            res.status(404).json({ err: err });
            next(err);
        });
};

exports.delete = (req, res, next) => {
    BankovniIzvod.destroy({
        where: {
            id: req.params.bankovniIzvodId
        }
    })
        .then(rowsDeleted => {
            if (rowsDeleted === 1) {
                res.status(200).json(Number(req.params.bankovniIzvodId));
            } else {
                res.status(404).json({ message: "Record not found" });
            }
        })
        .catch(err => {
            res.json({ err: err });
            next(err);
        });
}