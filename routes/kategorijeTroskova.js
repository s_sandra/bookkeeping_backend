const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const kategorijeTroskovaController = require("../controller/kategorijeTroskova");

router.get("/", checkAuth, kategorijeTroskovaController.index);
router.post("/", checkAuth, kategorijeTroskovaController.create);
//router.get("/:kategorijeTroskovaId", checkAuth, kategorijeTroskovaController.show);
router.put("/:kategorijeTroskovaId", checkAuth, kategorijeTroskovaController.update);
router.delete("/:kategorijeTroskovaId", checkAuth, kategorijeTroskovaController.delete);

module.exports = router;
