const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const ira = require("../controller/ira");

router.get("/", checkAuth, ira.index);
router.post("/", checkAuth, ira.create);
//router.get("/:iraId", checkAuth, ira.show);
router.put("/:iraId", checkAuth, ira.update);
router.delete("/:iraId", checkAuth, ira.delete);

module.exports = router;
