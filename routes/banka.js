const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const banka = require("../controller/banka");

router.get("/", checkAuth, banka.index);
router.post("/", checkAuth, banka.create);
//router.get("/:bankaId", checkAuth, banka.show);
router.put("/:bankaId", checkAuth, banka.update);
router.delete("/:bankaId", checkAuth, banka.delete);

module.exports = router;
