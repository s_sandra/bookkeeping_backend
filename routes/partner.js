const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const partnerController = require("../controller/partner");

router.get("/", checkAuth, partnerController.index);
router.post("/", checkAuth, partnerController.create);
//router.get("/:partnerId", checkAuth, partnerController.show);
router.put("/:partnerId", checkAuth, partnerController.update);
router.delete("/:partnerId", checkAuth, partnerController.delete);

module.exports = router;