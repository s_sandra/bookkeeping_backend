const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const userController = require("../controller/user");

/*const partneriRouter = require("./partners");
const bankaRouter = require("./banka");
const iraRouter = require("./ira");
const uraRouter = require("./ura");
const kategorijePrihodaRouter = require("./kategorijePrihoda");
const kategorijeTroskovaRouter = require("./kategorijeTroskova");
const kpiRouter = require("./kpi");
const porezRouter = require("./porez");*/

router.post("/login", userController.login);
router.post("/create", userController.create);

/*router.use(partneriRouter);
router.use(bankaRouter);
router.use(iraRouter);
//router.use(uraRouter);
router.use(kategorijePrihodaRouter);
router.use(kategorijeTroskovaRouter);
router.use(kpiRouter);
router.use(porezRouter);*/

module.exports = router;
