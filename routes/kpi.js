const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const kpi = require("../controller/kpi");

router.get("/", checkAuth, kpi.index);
router.post("/", checkAuth, kpi.create);
//router.get("/:kpiId", checkAuth, kpi.show);
router.put("/:kpiId", checkAuth, kpi.update);
router.delete("/:kpiId", checkAuth, kpi.delete);

module.exports = router;
