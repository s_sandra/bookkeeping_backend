const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const uraController = require("../controller/ura");

router.get("/", checkAuth, uraController.index);
router.post("/", checkAuth, uraController.create);
//router.get("/:uraId", checkAuth, uraController.show);
router.put("/:uraId", checkAuth, uraController.update);
router.delete("/:uraId", checkAuth, uraController.delete);

module.exports = router;
