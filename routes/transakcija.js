const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const transakcija = require("../controller/transakcija");

router.get("/", checkAuth, transakcija.index);
router.post("/", checkAuth, transakcija.create);
router.put("/:transakcijaId", checkAuth, transakcija.update);
router.delete("/:transakcijaId", checkAuth, transakcija.delete);

module.exports = router;



