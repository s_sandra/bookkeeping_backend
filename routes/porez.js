const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const porez = require("../controller/porez");

router.get("/", checkAuth, porez.index);
router.post("/", checkAuth, porez.create);
//router.get("/:porezId", checkAuth, porez.show);
router.put("/:porezId", checkAuth, porez.update);
router.delete("/:porezId", checkAuth, porez.delete);

module.exports = router;
