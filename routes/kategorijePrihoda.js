const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const kategorijePrihoda = require("../controller/kategorijePrihoda");

router.get("/", checkAuth, kategorijePrihoda.index);
router.post("/", checkAuth, kategorijePrihoda.create);
//router.get("/:kategorijePrihodaId", checkAuth, kategorijePrihoda.show);
router.put("/:kategorijePrihodaId", checkAuth, kategorijePrihoda.update);
router.delete("/:kategorijePrihodaId", checkAuth, kategorijePrihoda.delete);

module.exports = router;
