const router = require("express").Router();
const checkAuth = require("../middleware/check-auth");
const bankovniIzvod = require("../controller/bankovniIzvod");

router.get("/", checkAuth, bankovniIzvod.index);
router.post("/", checkAuth, bankovniIzvod.create);
//router.get("/:bankovniIzvodId", checkAuth, bankovniIzvod.show);
router.put("/:bankovniIzvodId", checkAuth, bankovniIzvod.update);
router.delete("/:bankovniIzvodId", checkAuth, bankovniIzvod.delete);

module.exports = router;